﻿using phoneBook.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace phoneBook.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {

            List<EmployeeModel> employee = new List<EmployeeModel>();
            employee = GetEmployee("");
            return View(employee);
        }
       

        public ActionResult Add()
        {   
            List<tDepartment> AllDepartment = new List<tDepartment>();

            using (PhoneBookDBEntities dc = new PhoneBookDBEntities())
            {
                AllDepartment = dc.tDepartments.OrderBy(a => a.DepName).ToList();

            }

            ViewBag.DepartmentID = new SelectList(AllDepartment, "DepartmentID", "DepName");


            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(tEmployee e)
        {
            #region
            List<tDepartment> AllDepartment = new List<tDepartment>();
            using(PhoneBookDBEntities dc = new PhoneBookDBEntities())
            {
                AllDepartment = dc.tDepartments.OrderBy(a => a.DepName).ToList();
            }
            ViewBag.DepartmentID = new SelectList(AllDepartment, "DepartmentID", "DepName", e.DepartmentID);


            #endregion

            #region
            if (ModelState.IsValid)
            {
                using(PhoneBookDBEntities dc = new PhoneBookDBEntities())
                {
                    dc.tEmployees.Add(e);
                    dc.SaveChanges();
                }
                return RedirectToAction("Index");
            }
            else
            {
                return View(e);
            }
            #endregion
        }

        public ActionResult View(int id)
        {

            tEmployee e = null;
            e = GetEmployee(id);
            return View(e);
        }

        private tEmployee GetEmployee(int employeeID)
        {
            tEmployee employee = null;
            using(PhoneBookDBEntities dc = new PhoneBookDBEntities())
            {
                
                var v = (from a in dc.tEmployees
                         join b in dc.tDepartments on a.DepartmentID equals b.DepartmentID
                         where a.EmployeeID.Equals(employeeID)
                         select new
                         {
                             a,
                             b.DepName
                         }).FirstOrDefault();
                if(v != null)
                {
                    employee = v.a;
                    employee.DepName = v.DepName;
                }
            }
           
            return employee;
        }

        public ActionResult Edit(int id)
        {
            tEmployee e = null;
            e = GetEmployee(id);

            if(e == null)
            {
                return HttpNotFound("Employee Not Found");
            }

            List<tDepartment> AllDepartment = new List<tDepartment>();
            using (PhoneBookDBEntities dc = new PhoneBookDBEntities())
            {
                AllDepartment = dc.tDepartments.OrderBy(a => a.DepName).ToList();
            }

            ViewBag.DepartmentID = new SelectList(AllDepartment, "DepartmentID", "DepName", e.DepartmentID);
            return View(e);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(tEmployee e)
        {
            #region
            List<tDepartment> AllDepartment = new List<tDepartment>();
            using (PhoneBookDBEntities dc = new PhoneBookDBEntities())
            {
                AllDepartment = dc.tDepartments.OrderBy(a => a.DepName).ToList();
            }

            ViewBag.DepartmentID = new SelectList(AllDepartment, "DepartmentID", "DepName", e.DepartmentID);
            #endregion

            #region
            if (ModelState.IsValid)
            {
                using (PhoneBookDBEntities dc = new PhoneBookDBEntities())
                {
                    var v = dc.tEmployees.Where(a => a.EmployeeID.Equals(e.EmployeeID)).FirstOrDefault();
                    if (v != null)
                    {
                        v.FirstName = e.FirstName;
                        v.LastName = e.LastName;
                        v.Title = e.Title;
                        v.Internal = e.Internal;
                        v.DepartmentID = e.DepartmentID;
                    }
                    dc.SaveChanges();
                }
                return RedirectToAction("Index");
            }else
            {
                return View(e);
            }
            #endregion
        }

        public ActionResult Delete(int id)
        {
            tEmployee e = null;
            e = GetEmployee(id);
            return View(e);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Delete")]
        public ActionResult DeleteConfirm(int id)
        {

            using(PhoneBookDBEntities dc = new PhoneBookDBEntities())
            {
                var employee = dc.tEmployees.Where(a => a.EmployeeID.Equals(id)).FirstOrDefault();
                if( employee != null)
                {
                    dc.tEmployees.Remove(employee);
                    dc.SaveChanges();
                    return RedirectToAction("Index");
                }else
                {
                    return HttpNotFound("Employee not found");
                }
            }
        }

        public ActionResult GridSearch()
        {
            List<EmployeeModel> employee = new List<EmployeeModel>();
            employee = GetEmployee("");
            return View(employee);
        }
        [HttpPost]
        public ActionResult Index(string SearchData)
        {
            List<EmployeeModel> employee = new List<EmployeeModel>();
            employee = GetEmployee(SearchData);
            return View(employee);
        }

        public List<EmployeeModel> GetEmployee(string SearchData)
        {
            List<EmployeeModel> lst = new List<EmployeeModel>();
            PhoneBookDBEntities db = new PhoneBookDBEntities();
            var DataItem = from data in db.tEmployees
                           join data2 in db.tDepartments on data.DepartmentID equals data2.DepartmentID
                           where SearchData == "" ? true : data.FirstName.StartsWith(SearchData)
                           select new EmployeeModel
                           {


                               EmployeeID = data.EmployeeID,
                               Title = data.Title,
                               FirstName = data.FirstName,
                               LastName = data.LastName,
                               Internal = data.Internal,
                               DepName = data2.DepName

                           };
            foreach(var a in DataItem)
            {
                lst.Add(new EmployeeModel
                {
                    EmployeeID = a.EmployeeID,
                    Title = a.Title,
                    FirstName = a.FirstName,
                    LastName = a.LastName,
                    Internal = a.Internal,
                    DepName = a.DepName
                });
            }

                
                
            return lst;
        }

        public ActionResult Export()
        {
            List<EmployeeModel> AllEmployee = new List<EmployeeModel>();
            using(PhoneBookDBEntities dc = new PhoneBookDBEntities())
            {
                var v = (from a in dc.tEmployees
                         join b in dc.tDepartments on a.DepartmentID equals b.DepartmentID
                         select new EmployeeModel
                         {
                             EmployeeID = a.EmployeeID,
                             Title = a.Title,
                             FirstName = a.FirstName,
                             LastName = a.LastName,
                             Internal = a.Internal,
                             DepName = b.DepName
                         }).ToList();

                AllEmployee = v;
            }
            
            return View(AllEmployee);
        }
        [HttpPost]
        [ActionName("Export")]
        public FileResult ExportData()
        {
            List<EmployeeModel> AllEmployee = new List<EmployeeModel>();
            using (PhoneBookDBEntities dc = new PhoneBookDBEntities())
            {
                var v = (from a in dc.tEmployees
                         join b in dc.tDepartments on a.DepartmentID equals b.DepartmentID
                         select new EmployeeModel
                         {
                             EmployeeID = a.EmployeeID,
                             Title = a.Title,
                             FirstName = a.FirstName,
                             LastName = a.LastName,
                             Internal = a.Internal,
                             DepName = b.DepName
                         }).ToList();

                AllEmployee = v;
            }
            var grid = new WebGrid(source: AllEmployee, canPage: false, canSort: false);
            string exportData = grid.GetHtml(
                                    columns: grid.Columns(
                                        grid.Column("Title", "Title"),
                                        grid.Column("FirstName", "First Name"),
                                        grid.Column("LastName", "Last Name"),
                                        grid.Column("Internal", "Internal"),
                                        grid.Column("DepName", "Department")
                                        )
                                ).ToHtmlString();
            return File(new System.Text.UTF8Encoding().GetBytes(exportData), "application/vnd.ms-excel", "PhoneBook.xls");
        }
    }
}