﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace phoneBook
{
    public class EmployeeValidation
    {

        [Display(Name ="Title")]
        public string Title { get; set; }

        [Display(Name ="First Name")]
        [Required(ErrorMessage ="Please Enter Employee's First Name")]
        public string FirstName { get; set; }

        [Display(Name ="Last Name")]
        [Required(ErrorMessage ="Please Enter Employee's Last Name")]
        public string LastName { get; set; }

        [Display(Name ="Interval")]
        [Required(ErrorMessage ="Please Enter Internal", AllowEmptyStrings = false)]
        public int Internal { get; set; }

        [Display(Name ="Department")]
        [Required(ErrorMessage ="Please Select Department")]
        public int DepartmentID { get; set; }

    }


    [MetadataType(typeof(EmployeeValidation))]
    public partial class tEmployee
    {

    }
}