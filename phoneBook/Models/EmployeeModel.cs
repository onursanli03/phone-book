﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace phoneBook.Models
{
    public class EmployeeModel
    {   
        public string Title { get; set; }
        public int EmployeeID { get; set; }
        public string FirstName { get; set;}
        public string LastName { get; set; }
        public int Internal { get; set; }
        public string DepName { get; set; }

        
    }
}